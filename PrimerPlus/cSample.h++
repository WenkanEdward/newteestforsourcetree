//
// Created by 12451 on 1/22/2020.
//

#ifndef PRIMERPLUS_CSAMPLE_H
#define PRIMERPLUS_CSAMPLE_H


#include<iostream>

using namespace std;

class CSample {
private:
    int a;
public:
    CSample() {
        cout << "1"<< endl;
    }
    CSample(int n){
        a = n;
        cout << "2"<<endl;
    }
    CSample( int p ,int q){
        cout << "3" << endl;
    }
    CSample( const CSample & c){
        a = c.a;
        cout << "Your copy is successfully!"<<endl;
    }

    virtual~CSample() {};
};


#endif //PRIMERPLUS_CSAMPLE_H
