//
// Created by 12451 on 1/23/2020.
//

#ifndef PRIMERPLUS_A_H
#define PRIMERPLUS_A_H


#include<iostream>

using namespace std;

class A {
private:
    int a;
public:
    A() {};
    void hello (){
        cout << "Hello!" <<endl;
    }
//    void hello(A  *this){        //会比原来的函数多一个参数！
//        cout << "Hello!" <<endl;
//    }//this 指针的使用
    virtual~A() {};
};


#endif //PRIMERPLUS_A_H
